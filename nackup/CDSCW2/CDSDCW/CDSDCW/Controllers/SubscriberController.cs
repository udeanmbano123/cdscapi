﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
//using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Xml.Serialization;
using System.IO;
using RestSharp;
using CDSDCW.Oracle;
using System.Net.Http;
using CDSDCW.BACKEND;
using System.Data.Entity.Migrations;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;

namespace CDSDCW.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")] // tune to your needs

    public class SubscriberController : ApiController
    {
        public BackEndCDSEntities  BackC=  new BackEndCDSEntities();
    
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Usr")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> Usr()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/users");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            users result = null;
            List<Oracle.users> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.users));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.users)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

              return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtcda")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datacda()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtcda");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_CDAs result = null;
            List<Oracle.dATASET_CDAs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_CDAs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_CDAs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
          
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dthold/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datahold(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dthold/"+s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
           dATASET_HOLDINGSs result = null;
            List<Oracle.dATASET_HOLDINGSs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_HOLDINGSs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_HOLDINGSs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

            return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
            
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtissue")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dataissue()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtissue");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_ISSUERSs result = null;
            List<Oracle.dATASET_ISSUERSs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_ISSUERSs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_ISSUERSs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

              return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtprices")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dataprices()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtprices");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_PRICESs result = null;
            List<Oracle.dATASET_PRICESs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_PRICESs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_PRICESs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

              return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
            
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtpricesG")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datapricesG()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtpricesG");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_PRICESs result = null;
            List<Oracle.dATASET_PRICESs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_PRICESs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_PRICESs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

            return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });

           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtpricesL")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datapricesL()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtpricesL");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_PRICESs result = null;
            List<Oracle.dATASET_PRICESs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_PRICESs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_PRICESs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

             return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });

           
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtclients/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dataclient(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtclients/"+s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            eSCROW_CLIENTSs result = null;
            List<Oracle.eSCROW_CLIENTSs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.eSCROW_CLIENTSs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.eSCROW_CLIENTSs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

             return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtport/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dataport(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtport/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            portfolioes result = null;
            List<Oracle.portfolioes> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.portfolioes));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.portfolioes)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtnumber/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> dataclientn(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnumber/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return await System.Threading.Tasks.Task.Run(() =>
            {

               return validate;
            });
            
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtnames/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> dataclientnn(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnames/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return await System.Threading.Tasks.Task.Run(() =>
            {

             return validate;
            });
            
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtcprice/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> dataclientnnn(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtcprice/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return await System.Threading.Tasks.Task.Run(() =>
            {

            return validate;
            });
            
        }
        public string number(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnumber/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return validate;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtid/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> idnumber2(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/did/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return await System.Threading.Tasks.Task.Run(() =>
            {

               return validate;
            });
           
        }
        public string idnumber(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/did/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return validate;
        }

        public string emails(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/demail/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            if (validate==null)
            {
                validate = "None";
            }
            return validate;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("demail/{s}")]
        [System.Web.Http.AllowAnonymous]
        public string emails22(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/demail/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            if (validate == null)
            {
                validate = "None";
            }
            return validate;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtstmt/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datastmt(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtstmt/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            eSCROW_MINI_STMTs result = null;
            List<Oracle.eSCROW_MINI_STMTs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.eSCROW_MINI_STMTs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.eSCROW_MINI_STMTs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

               return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Sub/{mem}/{toAdd}/{toRemove}")]
        public async Task<string> SubscribeW(string mem, string toAdd, string toRemove)
        {
            string res = "";
            try
            {
                int prodid = 0;
                int memid = 0;
                //get mem id
                var e = BackC.Subscribers.ToList().Where(a => a.PhoneNumber.ToLower().Replace(" ", "") == mem.ToLower().Replace(" ", ""));
                foreach (var z in e)
                {
                    memid = z.Id;
                }
                //get [product id
                var f = BackC.Products.ToList().Where(a => a.Name.ToLower().Replace(" ", "") == toAdd.ToLower().Replace(" ", ""));
                foreach (var z in f)
                {
                    prodid = z.IDENTITY;
                }

                //check combination

                var chk = 0;
                try
                {
                    chk = BackC.SubscriberProducts.ToList().Where(a => a.ProductId == prodid && a.SubscriberId == memid).Count();

                }
                catch (Exception)
                {

                    chk = 0;
                }
                if (chk == 0)
                {
                    double six = 0;
                    try
                    {
                        six = Convert.ToDouble(number(mem).Substring(0,3));

                    }
                    catch (Exception)
                    {

                        six = 0;
                    }
                    var p = BackC.Operators.ToList().Where(a=>a.Prefix==six);
                    string tests = "";
                    foreach (var z in p)
                    {
                        tests = z.Operator1;
                    }
             

                    //insert into Subscriber Products
                    BACKEND.SubscriberProduct my = new BACKEND.SubscriberProduct();
                    my.ProductId = prodid;
                    my.SubscriberId = memid;
                    my.Date = DateTime.Now;
                    if (tests == "Airtel")
                    {
                        my.Response = DirectDebitForOBOPAY_12SEP_Test("MAkibadrUserName", "MAkiba123#Password", "+254" + number(mem), 1, "254731214472");

                    }
                    else if (tests == "Safaricom")
                    {
                        var client = new RestClient("http://localhost:58888/EscrowService/rest/UserService/simulate/272007/CustomerPayBillOnline/1/254" + number(mem)+"/1");
                        var request = new RestRequest("", Method.POST);
                        IRestResponse response = client.Execute(request);
                        string validate = response.Content;
                        validate = validate.Replace(@"""", "");
                        my.Response = validate;
                    }
                    else
                    {
                        my.Response = "Failed";
                    }
                        BackC.SubscriberProducts.Add(my);
                    await BackC.SaveChangesAsync();
                    res = "Product was added successfully";

                  
                }
                else
                {
                    res = "Already subscribed";
                }
            }
            catch (Exception)
            {

                res = "Failed to add product";
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Register/{username}/{password}/{membernumber}/{regid}")]
        public async Task<string> Register(string username, string password,string membernumber,string regid)
        {//check if email exsists
            membernumber = membernumber.Replace("-", "*");
            regid = regid.Replace("-", "/");
            string s = membernumber;
            string add = "";
            s = s.Replace("-", "*");
            if (s.Length < 13)
            {
                int diff = 13 - s.Length;
                for (int x = 0; x < diff; x++)
                {
                    add = add + "0";
                }
                s = add + s;
            }
            membernumber = s;
            //check cdsnumber
            string res = "";
            int myy = 0;
            int nn = 0;
            int ee = 0;
            string sms = "";
            string allowedChars = "";
           allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 4; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            try
            {
                myy = BackC.Subscribers.ToList().Where(a => a.PhoneNumber == membernumber).Count();

            }
            catch (Exception)
            {

                myy = 0;
            }
    
          
             if (myy > 0)
                {
                    res = "Duplicate Client numbers are not allowed";
                }
                else if (emails(membernumber)=="None")
                {
                    res = "Email does not exsist";
                }
                else if (regid != idnumber(membernumber))
                {
                    res = "ID does not exsist";
                }
                else if (myy==0)
                {//pad zeros to length
                try
                {
                    var resp = BackC.ParameterCharges.ToList().Where(a => a.Parametername.ToLower() == ("REGISTER").ToLower());
                    decimal cm = 0;
                    foreach (var z in resp)
                    {
                        cm = z.charge;
                    }
                    BACKEND.Subscriber my = new BACKEND.Subscriber();
                    my.Username = regid;
                    my.Password = password;
                    my.Email = emails(membernumber);
                    my.PhoneNumber = membernumber;
                    my.DateTime = DateTime.Now;
                    my.Active = true;

                    my.Code = passwordString;
                    double six = 0;
                    try
                    {
                        six = Convert.ToDouble(number(membernumber).Substring(0, 3));

                    }
                    catch (Exception)
                    {

                        six = 0;
                    }
                    var p = BackC.Operators.ToList().Where(a => a.Prefix == six);
                    string tests = "";
                    foreach (var z in p)
                    {
                        tests = z.Operator1;
                    }

                    if (tests == "Airtel")
                    {
                        my.Status = DirectDebitForOBOPAY_12SEP_Test("MAkibadrUserName", "MAkiba123#Password", "+254" + number(membernumber), cm, "254731214472");

                    }
                    else if (tests == "Safaricom")
                    {
                       
                            var client = new RestClient("http://localhost:58888/EscrowService/rest/UserService/simulate/272007/CustomerPayBillOnline/" + cm + "/254" + number(membernumber) + "/1150");
                            var request = new RestRequest("", Method.POST);
                            IRestResponse response = client.Execute(request);
                            string validate = response.Content;
                            validate = validate.Replace(@"""", "");
                            my.Status = validate;
                  
                    }
                    else
                    {
                        my.Status = "Failed";
                    }
                    BackC.Subscribers.AddOrUpdate(my);
                    await BackC.SaveChangesAsync();
                    string refz = "Thank you for registering on our mobile application " + my.Username + " 4 digit code " + my.Code;
                SendMail2(my.Email, refz, "CDSC Registration");
 res = "Successfully registered";
                   
                }           
                catch (Exception g)
                {

                    res = g.ToString();
                }
                    //subscribe

                 }




            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Products")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> nemes()
        {
           dynamic products = null;
            try
            {
              products = (from m in BackC.Products
                          select new { m.Name }).ToList();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                
                return await System.Threading.Tasks.Task.Run(() =>
                {

                    return (dynamic)raise.ToString();
                });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return products;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Subscribed/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> neme(string s){
            int my = 0;
            var sx = BackC.Subscribers.ToList().Where(a=>a.PhoneNumber==s);
            foreach (var e in sx)
            {
                my = e.Id;
            }
            var products = from m in BackC.SubscriberProducts
                           join v in BackC.Products on m.ProductId equals v.IDENTITY
                           where m.SubscriberId == my
                           select new { v.Name };
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return products;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Change/{s}/{p}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LoginMemPut(string s, string p)
        {
            s = s.Replace("-", "*");
            string res = "";
            int my = 0;
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber == s
                    select v;
            foreach (var k in c)
            {
                my = k.Id;

   try
            {
                BACKEND.Subscriber nw = BackC.Subscribers.Find(my);
                nw.Password = p;
                BackC.Subscribers.AddOrUpdate(nw);

                res = "Password was saved successfully";
            }

            catch (Exception f)
            {

                res = f.Message;
            }
                
            }
         
            await BackC.SaveChangesAsync();
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        //Profiles update
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Profile/{username}/{email}/{csdnumber}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LoginMemPutUU(string username, string email,string csdnumber)
        {
            csdnumber = csdnumber.Replace("-", "*");
            string res = "";
            int my = 0;
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber == csdnumber
                    select v;

            //ccheck if username or email exsists
            var nn = BackC.Subscribers.ToList().Where(a => a.Username == username && a.PhoneNumber!=csdnumber).Count();
            var ss= BackC.Subscribers.ToList().Where(a => a.Email== email && a.PhoneNumber != csdnumber).Count();
            if (nn > 0)
            {
                res = "Username is already taken";
            }
            else if (ss > 0)
            {
                res = "Email is already taken";
            }
            else
            {


                foreach (var k in c)
                {
                    my = k.Id;

                    try
                    {
                        BACKEND.Subscriber nw = BackC.Subscribers.Find(my);
                        nw.Username = username;
                        nw.Email = email;
                        BackC.Subscribers.AddOrUpdate(nw);

                        res = "Password was saved successfully";
                    }

                    catch (Exception f)
                    {

                        res = f.Message;
                    }

                }

               await BackC.SaveChangesAsync();
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            }); ;
        }
        //Profile Accept
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("ProfileAccept/{csdnumber}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> LoginMemPutUUA(string csdnumber)
        {
            csdnumber = csdnumber.Replace("-", "*");
            string res = "";
            int my = 0;
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber == csdnumber
                    let Name=v.Username
                    let Mail=v.Email
                    select new { Name,Mail };
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return c;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Ammend/{email}/{mobile}/{postal}/{cds}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> Ammend(string email,string mobile,string postal,string cds)
        {
            postal = postal.Replace("%20"," ");
            postal = postal.Replace("-", "/");
            string res = "Successfully added";
            try
            {
                Ammend my = new BACKEND.Ammend();
                my.Email = email;
                my.Mobile = mobile;
                my.Postal = postal;
                my.existingcds = cds;
                my.capture = "CAPTURE";
                BackC.Ammends.AddOrUpdate(my);
                await BackC.SaveChangesAsync();
            }
            catch (Exception)
            {

                res = "Failed to save update";
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("CDSAcc/{postal}/{Surname}/{Forenames}/{Title}/{IDNoPP}/{DateOfBirth}/{Nationality}/{Mobile}/{Email}/{Bank}/{Account_Number}/{cdsaccount}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> cdsacc(string postal, string Surname,string Forenames,string Title,string IDNoPP,string DateOfBirth, string Nationality,string Mobile, string Email,string Bank,string Account_Number, string cdsaccount)
        { 
         string res = "Successfully createdaccount , move onto to step 2 to add documents";
            postal = postal.Replace("%20", " ");
            Bank = Bank.Replace("%20", " ");
            int max = 0;
            try
            {
                max = BackC.CDSAccounts.ToList().Max(a => a.CDSAccountID);
            }
            catch (Exception)
            {

                max = 0;
            }
            try
            {
                CDSAccount my = new BACKEND.CDSAccount();
                my.postal = postal;
                my.Surname = Surname;
                my.Forenames = Forenames;
                my.Title = Title;
                my.IDNoPP = IDNoPP;
                my.DateOfBirth = Convert.ToDateTime(DateOfBirth);
                my.Nationality = Nationality;
                my.Mobile = Mobile;
                my.Email =Email;
                my.Bank = Bank;
                my.Account_Number = Account_Number;
                my.cdsaccount1 = cdsaccount;
                BackC.CDSAccounts.AddOrUpdate(my);
                await BackC.SaveChangesAsync();
            }
            catch (Exception)
            {

                res = "Failed to save account";
            }
            
            if (res=="Successfully createdaccount , move onto to step 2 to add documents")
            {
                res = res + ":" + (max + 1).ToString();
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AddDocument/{doc_generated}/{ContentType}/{Data}/{CDSAccountID}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> GetNotifications()
        {
            var cc = BackC.GeneralMessages.ToList().Where(a=>a.expiryDate<=DateTime.Now);

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return cc;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AddDocument/{doc_generated}/{ContentType}/{Data}/{CDSAccountID}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> AccDD(string doc_generated,string ContentType,byte[] Data, string CDSAccountID)
        {
         
          string res = "Successfully added document";
            try
            {
                Accounts_Documents my = new BACKEND.Accounts_Documents();
                my.doc_generated = doc_generated;
                my.ContentType = ContentType;
                my.Data = Data;
                my.CDSAccountID = Convert.ToInt32(CDSAccountID);
                BackC.Accounts_Documents.AddOrUpdate(my);
                await BackC.SaveChangesAsync();
            }
            catch (Exception)
            {

                res = "Failed to save update";
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("LoginMem/{s}/{p}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LoginMem(string s, string p)
        {
            string add = "";
            s = s.Replace("-", "*");
            if (s.Length < 13)
            {
                int diff = 13 - s.Length;
                for(int x = 0; x < diff; x++)
                {
                    add = add + "0";
                }
                s = add + s;
            }
            string res = "0";
            string mail = "";
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber == s && v.Password == p &&  v.Active==true
                    select v;
            foreach (var k in c)
            {
                res = k.PhoneNumber;
                mail = k.Email;
                res = "1";
            }
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 4; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            //Generate OTP PIN
            foreach (var k in c)
            {
                k.Code = passwordString;
            }
            await BackC.SaveChangesAsync();
            //email the code
            string refz = "Your OTP PIN is "+ passwordString;
            if (mail!=null)
            {
          SendMail2(mail, refz, "CDSC OTP PIN LOGIN");
                res = "1";
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Link/{s}/{m}/{t}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LinkAccounts(string s,string m,string t)
        {
            var gv = BackC.Subscribers.ToList().Where(a=>a.PhoneNumber==s);
            int hv = 0;
            foreach (var q in gv){
                hv = q.Id;
            }
            if (isValid(m,t))
            {
                //ding linked accounts
                LinkedAccount nm = new LinkedAccount();
                nm.CSD = m;
                nm.mobile = t;
                nm.SubscriberRef = s;
                nm.SubscriberID = hv;
                BackC.LinkedAccounts.Add(nm);
               await BackC.SaveChangesAsync();
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return "success";
            });
            
        }

        public bool isValid(string m,string t)
        {
            return true;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("OtpV/{s}/{m}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> ValidOtp(string s,string m)
        { string response = "0";
            try
            {
               
                string add = "";
                s = s.Replace("-", "*");
                if (s.Length < 13)
                {
                    int diff = 13 - s.Length;
                    for (int x = 0; x < diff; x++)
                    {
                        add = add + "0";
                    }
                    s = add + s;
                }
                var c = BackC.Subscribers.ToList().Where(a => a.PhoneNumber == s && a.Code == m).Count();

                if (c > 0)
                {
                    response = "1";
                }
                else
                {
                    response = "0";
                }
            }
            catch (Exception)
            {

                response = "0";
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return response;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Otp/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string>  ResendOtp(string s)
        {
            s = s.Replace("-", "*");
            string add = "";
            s = s.Replace("-", "*");
            if (s.Length < 13)
            {
                int diff = 13 - s.Length;
                for (int x = 0; x < diff; x++)
                {
                    add = add + "0";
                }
                s = add + s;
            }
            string res = "";
            string mail = "";
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber==s
                    select v;
           
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 4; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            //Generate OTP PIN
            foreach (var k in c)
            {
                k.Code = passwordString;
                mail = k.Email;
            }
            await BackC.SaveChangesAsync();
            //email the code
            string refz = "Your OTP PIN is " + passwordString;
            SendMail2(mail, refz, "CDSC OTP PIN LOGIN");
            res = "1";
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("AppUsage/{a}/{m}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> AppUsage(string a, string m)
        {
            a = a.Replace("-"," ");
            string res = "Done";
            BACKEND.AppUsage my = new BACKEND.AppUsage();
            my.UserId = m;
            my.Activity = a;
            my.Date = DateTime.Now;
            BackC.AppUsages.Add(my);
            await BackC.SaveChangesAsync();
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Forg/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async  Task<string> LoginMemPutL(string s)
        {
            s = s.Replace("-", "*");
            string res = "";
            int my = 0;
            string mailm = "";
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber.ToLower() == s.ToLower()
                    select v;
            foreach (var k in c)
            {
                my = k.Id;
                mailm = k.Email;
            }
            string sms = "";
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 8; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
         
            try
            {
                BACKEND.Subscriber nw = BackC.Subscribers.Find(my);
                nw.Password = ComputeHash(passwordString, new SHA256CryptoServiceProvider());
                BackC.Subscribers.AddOrUpdate(nw);
               await BackC.SaveChangesAsync();
                res = "Password was reset successfully "+ passwordString;
                //send sms
                SendMail2(mailm,res,"CDSC MOBILE PasswordReset");
            }
            catch (Exception)
            {

                res = "Password was not saved";
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Deactivate/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LoginMemPutLL(string s)
        {
            s = s.Replace("-", "*");
            string res = "";
            int my = 0;
            string mailm = "";
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber.ToLower() == s.ToLower()
                    select v;
            string names = "";
            foreach (var k in c)
            {
                my = k.Id;
                mailm = k.Email;
                names = k.Username;
            }
            string sms = "";
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 14; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            try
            {
                Subscriber nw = BackC.Subscribers.Find(my);
                nw.Active = false;
                BackC.Subscribers.AddOrUpdate(nw);
                await BackC.SaveChangesAsync();
                res = "Dear "+ names + "\n\n Account number "+ s +" has been deactivated on CDSC MOBILE APP";
                //send sms
                try
                {
                    SendMail2(mailm, res, "CDSC Mobile Account Deactivation");

                }
                catch (Exception)
                {

                    
                }  }
            catch (Exception)
            {

                res = "Password was not saved";
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
        public void SendMail2(string id, string refz, string name)
        {
            //SendMail(email,refz,HttpUtility.UrlDecode(name));
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
delegate (object s, X509Certificate certificate,
       X509Chain chain, SslPolicyErrors sslPolicyErrors)
{ return true; };
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");

            MailMessage mm = new MailMessage("edocwebapp@gmail.com", id, "CDSC MOBILE APP NOTIFICATION " + name, refz);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(mm);

        }
        //payment methods
        public string DirectDebitForOBOPAY_12SEP_Test(string Username, string Password, string CustMobile, decimal Amount, string MerchantMSISDN)
        {
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslerror) => true;

                var request = HttpWebRequest.Create("https://41.223.58.41:8446/Service1.asmx");
                byte[] bytes = null;
                string strXmlInputData = string.Empty;

                strXmlInputData += "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'>";
                strXmlInputData += "<soapenv:Header/>";
                strXmlInputData += "<soapenv:Body>";
                strXmlInputData += "<tem:DirectDebitAPI>";
                strXmlInputData += "<tem:UserName>" + Username + "</tem:UserName>";
                strXmlInputData += "<tem:Password>" + Password + "</tem:Password>";
                strXmlInputData += "<tem:CustomerMobileNumber>" + CustMobile + "</tem:CustomerMobileNumber>";
                strXmlInputData += "<tem:Amount>" + Amount + "</tem:Amount>";
                strXmlInputData += "<tem:MerchantWalletMsisdn>" + MerchantMSISDN + "</tem:MerchantWalletMsisdn>";
                strXmlInputData += "</tem:DirectDebitAPI>";
                strXmlInputData += "</soapenv:Body>";
                strXmlInputData += "</soapenv:Envelope>";
                bytes = System.Text.Encoding.ASCII.GetBytes(strXmlInputData);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                using (WebResponse myWebResponse = request.GetResponse())
                {
                    using (Stream myResponseStream = myWebResponse.GetResponseStream())
                    {
                        using (StreamReader myStreamReader = new StreamReader(myResponseStream))
                        {
                            return myStreamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException er)
            {
                writetofile(er.ToString());
                return "XX";
            }
            catch (Exception ex)
            {
                writetofile(ex.ToString());
                return "XX";
            }
        }
        public void writetofile(String mssg)
        {
            string fileName = "LogWriter"+DateTime.Now;
            StreamWriter objWriter3 = new System.IO.StreamWriter(fileName, true);
            objWriter3.WriteLine(DateTime.Now.ToString() + ":-Logged Message-: " + mssg);
            objWriter3.Close();
        }
        //writing enquiries

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("ticketN/{ticket}/{type}")]
        [AllowAnonymous]
        public async Task<dynamic> NotificationDetails(string ticket, string type)
        {
            var urls4 = BackC.FeedBackTrans.ToList();
            if (type == "Inbox")
            {
                urls4 = (from s in BackC.FeedBackTrans
                         where s.ticketRef == ticket && s.IsClientNotification == true
                         select s).ToList();

            }
            else if (type == "Sent")
            {
                urls4 = (from s in BackC.FeedBackTrans
                         where s.ticketRef == ticket && s.IsAppNotification == true
                         select s).ToList();
            }


            return await System.Threading.Tasks.Task.Run(() =>
            {

                return urls4;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("tickets/{ticket}/{response}")]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        public async Task<string> TicketResponse(string ticket, string response)
        {
            int id = 0;
            var tick = BackC.tblFeedBackMains.Where(a => a.ticketRef == ticket);
            foreach (var c in tick)
            {
                id = c.tblFeedBackMainID;
            }
            if (tick.Count() > 0)
            {
                try
                {
                    FeedBackTran my = new FeedBackTran();
                    //Debit
                    my.ticketRef = ticket;
                    my.FeedID = id;
                    my.responsemessage = HttpUtility.UrlDecode(response);
                    my.IsClientNotification = false;
                    my.IsAppNotification = true;
                    my.datereceived = DateTime.Now;
                    my.UserID = 0;
                   BackC.FeedBackTrans.Add(my);
                    //Update table  
                    await BackC.SaveChangesAsync();
                    //alert operator in charge
                    var d = BackC.tblFeedBackMains.ToList().Where(a => a.ticketRef == ticket);
                    string myD = "";
                    foreach (var c in d)
                    {
                        myD = c.logon;
                    }
                    int n = Convert.ToInt32(myD);
                    var p = BackC.Users.ToList().Where(a => a.UserId == n);
                    foreach (var v in p)
                    {
                        SendMail3(v.Email, "Ticket No." + ticket + ":" + my.responsemessage, v.FirstName + " " + v.LastName, ticket);

                    }

                    return await System.Threading.Tasks.Task.Run(() =>
                    {

                        return "Success";
                    });
                   
                }
                catch (Exception)
                {
                    return await System.Threading.Tasks.Task.Run(() =>
                    {

                       return "Ticket Reference number not found";
                    });
                    
                }
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

              return "Ticket Reference number not found";
            });
            
        }

       
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("ticketAdd/{feedback}/{mobile}")]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        public async Task<string> TicketNotification(string feedback,string mobile)
        {
            int? myD = 0;
            string sms = "";
            string eml = "";
            var c = BackC.Subscribers.ToList();
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnumber/" + mobile);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            foreach (var p in c)
            {
                eml = p.Email;
            }
            try
            {
                try
                {
                    myD = BackC.tblFeedBackMains.Max(a => a.tblFeedBackMainID);
                }
                catch (Exception)
                {

                    myD = 0;
                }

                var client2 = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnames/" + mobile);
                var request2 = new RestRequest("", Method.GET);
                IRestResponse response2 = client2.Execute(request2);
                string validate2 = response2.Content;
                validate2 = validate2.Replace(@"""", "");
                tblFeedBackMain my = new tblFeedBackMain();
                //Debit
               my.Feedback = HttpUtility.UrlDecode(feedback);
                my.Email = eml;
                my.mobile = validate;
                my.name = validate2;
                my.logon = "0";
                my.ticketstatus = "INCOMING";
                my.datereceived = DateTime.Now;
                my.ticketRef = "CDSC0000" + (myD + 1).ToString();
                BackC.tblFeedBackMains.Add(my);
                //Update table  
                await BackC.SaveChangesAsync();
                string refz = "Thank you for using our CDSC MOBILE APP your ticket Number is " + my.ticketRef;
                //sms = SendSMS(refz, mobile);
                SendMail4(eml, refz, HttpUtility.UrlDecode(my.name));
                return await System.Threading.Tasks.Task.Run(() =>
                {

                    return  "Success:" + my.ticketRef;
                });
                
            }
            catch (Exception e)
            {
                return await System.Threading.Tasks.Task.Run(() =>
                {

                   return e.ToString();
                });
                
            }
        }

        public void SendMail3(string id, string refz, string name, string ticket)
        {
            //SendMail(email,refz,HttpUtility.UrlDecode(name));
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
delegate (object s, X509Certificate certificate,
       X509Chain chain, SslPolicyErrors sslPolicyErrors)
{ return true; };
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");

            MailMessage mm = new MailMessage("edocwebapp@gmail.com", id, "CDSC MOBILE Ticket Updates: " + ticket + " " + name, refz);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(mm);

        }
        public  void SendMail4(string id, string refz, string name)
        {
            //SendMail(email,refz,HttpUtility.UrlDecode(name));
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
delegate (object s, X509Certificate certificate,
       X509Chain chain, SslPolicyErrors sslPolicyErrors)
{ return true; };
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");

            MailMessage mm = new MailMessage("edocwebapp@gmail.com", id, "CDSC MOBILE APP NOTIFICATION " + name, refz);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(mm);

        }

    }
}
