package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "ESCROWCLIENTS ") 
public class ESCROW_CLIENTS implements Serializable {  
	   private static final long serialVersionUID = 1L; 
	   public ESCROW_CLIENTS() {}
	   
	public ESCROW_CLIENTS(String mEMBER_CODE, String mEMBER_TYPE, String cLIENT_PREFIX, String cLIENT_SUFFIX,
			String cLIENT_TYPE, String sURNAME, String oTHER_NAMES, String dATE_OF_REGISTRATION, String dATE_CHANGED) {
		super();
		MEMBER_CODE = mEMBER_CODE;
		MEMBER_TYPE = mEMBER_TYPE;
		CLIENT_PREFIX = cLIENT_PREFIX;
		CLIENT_SUFFIX = cLIENT_SUFFIX;
		CLIENT_TYPE = cLIENT_TYPE;
		SURNAME = sURNAME;
		OTHER_NAMES = oTHER_NAMES;
		DATE_OF_REGISTRATION = dATE_OF_REGISTRATION;
		DATE_CHANGED = dATE_CHANGED;
	}	

	public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}
	 @XmlElement 
	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}
	public String getMEMBER_TYPE() {
		return MEMBER_TYPE;
	}
	 @XmlElement 
	public void setMEMBER_TYPE(String mEMBER_TYPE) {
		MEMBER_TYPE = mEMBER_TYPE;
	}
	public String getCLIENT_PREFIX() {
		return CLIENT_PREFIX;
	}
	 @XmlElement 
	public void setCLIENT_PREFIX(String cLIENT_PREFIX) {
		CLIENT_PREFIX = cLIENT_PREFIX;
	}
	public String getCLIENT_SUFFIX() {
		return CLIENT_SUFFIX;
	}
	 @XmlElement 
	public void setCLIENT_SUFFIX(String cLIENT_SUFFIX) {
		CLIENT_SUFFIX = cLIENT_SUFFIX;
	}
	public String getCLIENT_TYPE() {
		return CLIENT_TYPE;
	}
	 @XmlElement 
	public void setCLIENT_TYPE(String cLIENT_TYPE) {
		CLIENT_TYPE = cLIENT_TYPE;
	}
	public String getSURNAME() {
		return SURNAME;
	}
	 @XmlElement 
	public void setSURNAME(String sURNAME) {
		SURNAME = sURNAME;
	}
	public String getOTHER_NAMES() {
		return OTHER_NAMES;
	}
	 @XmlElement 
	public void setOTHER_NAMES(String oTHER_NAMES) {
		OTHER_NAMES = oTHER_NAMES;
	}
	public String getDATE_OF_REGISTRATION() {
		return DATE_OF_REGISTRATION;
	}
	 @XmlElement 
	public void setDATE_OF_REGISTRATION(String dATE_OF_REGISTRATION) {
		DATE_OF_REGISTRATION = dATE_OF_REGISTRATION;
	}
	public String getDATE_CHANGED() {
		return DATE_CHANGED;
	}
	 @XmlElement 
	public void setDATE_CHANGED(String dATE_CHANGED) {
		DATE_CHANGED = dATE_CHANGED;
	}
	private String MEMBER_CODE;
	private String MEMBER_TYPE;
	private String CLIENT_PREFIX; 
	private String CLIENT_SUFFIX; 
	private String CLIENT_TYPE;
	private String SURNAME;
	private String OTHER_NAMES;
	private String DATE_OF_REGISTRATION;
	private String DATE_CHANGED;
	
	
}
