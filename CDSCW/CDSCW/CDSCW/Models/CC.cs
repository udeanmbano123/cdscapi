﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDSCW.Models
{
    public class CC
    {
        public string Company { get; set; }
        public string CDS_Number { get; set; }
        public string Shares { get; set; }
    }
    public class SafariTranss
    {
        public string MerchantRequestID { get; set; }
        public string ResponseCode { get; set; }
        public string CustomerMessage { get; set; }
        public string CheckoutRequestID { get; set; }
        public string ResponseDescription { get; set; }

    }
}