//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CDSCW.BACKEND
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product
    {
        public int IDENTITY { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public decimal Charge { get; set; }
        public System.DateTime Date { get; set; }
        public string code { get; set; }
    }
}
