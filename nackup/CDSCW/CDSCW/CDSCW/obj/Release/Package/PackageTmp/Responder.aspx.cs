﻿using CDSCW.BACKEND;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CDSCW
{
    public partial class Responder : System.Web.UI.Page
    {
        public BackEndCDSEntities BackC = new BackEndCDSEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                Page.Response.ContentType = "text/xml";
                System.IO.StreamReader reader = new System.IO.StreamReader(Page.Request.InputStream);
                string xmlData = reader.ReadToEnd();
                if (xmlData!=""|| xmlData !=null)
                {
                savelog(xmlData);
                }
               
                System.IO.StreamWriter SW = default(System.IO.StreamWriter);
                SW = File.CreateText(Server.MapPath("api\\xmlRec" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"));
                SW.WriteLine(xmlData);
                SW.Close();
                //Synch Back
                //synch();
                //get transactionstatus
            }
            catch (System.Net.WebException exp)
            {

                System.IO.StreamWriter SW = default(System.IO.StreamWriter);
                SW = File.CreateText(Server.MapPath("api\\error" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"));
                SW.WriteLine(exp.ToString());
                SW.Close();
            }
            

        }

        public void savelog(string s)
        {
            try
            {
                dynamic json = JObject.Parse(s);
                // dynamic o = JsonConvert.DeserializeObject(json.ToString());
               dynamic tra = json.Body.stkCallback.CheckoutRequestID;
                dynamic re= json.Body.stkCallback.ResultDesc;
                string transid = tra.ToString();
                string resp = re.ToString();

                string pay = "";
                string went = "";
              
                 
                        if (resp.Contains("successfully")==true)
                        {
                            pay = "PAID";
                            went = "SUCCESS";
                        }
                        else
                        {
                            pay = "NPAID";
                            went = "NSUCCESS";
                        }
                 
                //Update Customer record
                var up = BackC.Subscribers.ToList().Where(a => a.CheckoutRequestID.Trim() == transid.Trim()).FirstOrDefault();

                var my = BackC.Subscribers.Find(up.Id);
                my.runnS = pay;
                my.Status = went;
                if (went=="SUCCESS")
                {
                my.DateTime = DateTime.Now;
                my.subss = my.DateTime.AddYears(1);
                }else
                {
                    my.DateTime = my.DateTime;
                    my.subss = my.subss;
               }
                 BackC.Subscribers.AddOrUpdate(my);
                BackC.SaveChanges();
                //Update Safari
                var upp = BackC.SafariTrans.ToList().Where(a => a.CheckoutRequestID.Trim() == transid.Trim()).FirstOrDefault();
                var myy = BackC.SafariTrans.Find(upp.SafariTransID);
                myy.CallBack =s;
                BackC.SafariTrans.AddOrUpdate(myy);
                BackC.SaveChanges();
            }
            catch (Exception f)
            {
                System.IO.StreamWriter SW = default(System.IO.StreamWriter);
                SW = File.CreateText(Server.MapPath("api\\error" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"));
                SW.WriteLine(f.Message);
                SW.Close();

            }
        }


    }
}