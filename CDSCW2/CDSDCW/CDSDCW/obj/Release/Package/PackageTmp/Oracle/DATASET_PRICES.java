package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "DATASETPRICES") 
public class DATASET_PRICES implements Serializable {  
	   private static final long serialVersionUID = 1L; 
	   public DATASET_PRICES() {}
	public String getTrade_date() {
		return Trade_date;
	}
	 @XmlElement 
	public void setTrade_date(String trade_date) {
		Trade_date = trade_date;
	}
	public String getISSUER_CODE() {
		return ISSUER_CODE;
	}
	 @XmlElement 
	public void setISSUER_CODE(String iSSUER_CODE) {
		ISSUER_CODE = iSSUER_CODE;
	}
	public String getISIN() {
		return ISIN;
	}
	 @XmlElement 
	public void setISIN(String iSIN) {
		ISIN = iSIN;
	}
	public String getMAIN_TYPE() {
		return MAIN_TYPE;
	}
	 @XmlElement 
	public void setMAIN_TYPE(String mAIN_TYPE) {
		MAIN_TYPE = mAIN_TYPE;
	}
	public String getSUB_TYPE() {
		return SUB_TYPE;
	}
	 @XmlElement 
	public void setSUB_TYPE(String sUB_TYPE) {
		SUB_TYPE = sUB_TYPE;
	}
	public String getPRICE_HI() {
		return PRICE_HI;
	}
	 @XmlElement 
	public void setPRICE_HI(String pRICE_HI) {
		PRICE_HI = pRICE_HI;
	}
	public String getPRICE_LOW() {
		return PRICE_LOW;
	}
	 @XmlElement 
	public void setPRICE_LOW(String pRICE_LOW) {
		PRICE_LOW = pRICE_LOW;
	}
	public String getCLOSE_PRICE() {
		return CLOSE_PRICE;
	}
	 @XmlElement 
	public void setCLOSE_PRICE(String cLOSE_PRICE) {
		CLOSE_PRICE = cLOSE_PRICE;
	}
	public String getAVERAGE_PRICE() {
		return AVERAGE_PRICE;
	}
	 @XmlElement 
	public void setAVERAGE_PRICE(String aVERAGE_PRICE) {
		AVERAGE_PRICE = aVERAGE_PRICE;
	}
	public String getSHARE_VOLUME() {
		return SHARE_VOLUME;
	}
	 @XmlElement 
	public void setSHARE_VOLUME(String sHARE_VOLUME) {
		SHARE_VOLUME = sHARE_VOLUME;
	}
	public String getNum_of_trades() {
		return num_of_trades;
	}
	 @XmlElement 
	public void setNum_of_trades(String num_of_trades) {
		this.num_of_trades = num_of_trades;
	}
	public String getTURNOVER() {
		return TURNOVER;
	}
	 @XmlElement 
	public void setTURNOVER(String tURNOVER) {
		TURNOVER = tURNOVER;
	}
	private String Trade_date;
	public DATASET_PRICES(String trade_date, String iSSUER_CODE, String iSIN, String mAIN_TYPE, String sUB_TYPE,
			String pRICE_HI, String pRICE_LOW, String cLOSE_PRICE, String aVERAGE_PRICE, String sHARE_VOLUME,
			String num_of_trades, String tURNOVER) {
		super();
		Trade_date = trade_date;
		ISSUER_CODE = iSSUER_CODE;
		ISIN = iSIN;
		MAIN_TYPE = mAIN_TYPE;
		SUB_TYPE = sUB_TYPE;
		PRICE_HI = pRICE_HI;
		PRICE_LOW = pRICE_LOW;
		CLOSE_PRICE = cLOSE_PRICE;
		AVERAGE_PRICE = aVERAGE_PRICE;
		SHARE_VOLUME = sHARE_VOLUME;
		this.num_of_trades = num_of_trades;
		TURNOVER = tURNOVER;
	}
	private String ISSUER_CODE;
	private String ISIN;
	private String MAIN_TYPE;
	private String SUB_TYPE;
	private String PRICE_HI;
	private String PRICE_LOW;
	private String CLOSE_PRICE;
	private String AVERAGE_PRICE;
	private String SHARE_VOLUME;
	private String num_of_trades;
	private String TURNOVER;
}
