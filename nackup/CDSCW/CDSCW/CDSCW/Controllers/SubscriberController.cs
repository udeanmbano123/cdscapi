﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
//using System.Net.Mail;
using System.Web.Http;
using System.IO;
using System.Web.Http.Cors;
using System.Xml.Serialization;
using System;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using RestSharp;
using CDSCW.Oracle;
using System.Net.Http;
using CDSCW.BACKEND;
using System.Data.Entity.Migrations;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using CDSCW.Models;
using Newtonsoft.Json;

namespace CDSCW.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")] // tune to your needs

    public class SubscriberController : ApiController
    {
        public BackEndCDSEntities BackC =  new BackEndCDSEntities();
    
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Usr")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> Usr()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/users");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            users result = null;
            List<Oracle.users> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.users));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.users)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

              return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Tests")]
        [System.Web.Http.AllowAnonymous]
        public dynamic Usrss()
        {
            try
            {
                SafariTran mw = new SafariTran();
                mw.MerchantRequestID = "Airtel";
                mw.CheckoutRequestID = "+254";
                mw.CustomerMessage = "Airtel";
                mw.ResponseCode = "Airtel";
                mw.ResponseDescription = "Airtel";
                mw.CallBack ="Test";
                BackC.SafariTrans.AddOrUpdate(mw);
                BackC.SaveChangesAsync();
            }
            catch (Exception f)
            {

                return f.Message;
            }

            return "bho";
        }

    [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtcda")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datacda()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtcda");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_CDAs result = null;
            List<Oracle.dATASET_CDAs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_CDAs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_CDAs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
          
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtmember")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datacdas()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/members");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
          
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
                {
                    Content = new StringContent(validate, Encoding.UTF8, "application/xml")
                };
            });

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtbanks")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> databanks()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/banks");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
                {
                    Content = new StringContent(validate, Encoding.UTF8, "application/xml")
                };
            });

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dthold/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datahold(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/hold/"+s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
          return await System.Threading.Tasks.Task.Run(() =>
            {

            return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
            
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtissue")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dataissue()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtissue");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_ISSUERSs result = null;
            List<Oracle.dATASET_ISSUERSs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_ISSUERSs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_ISSUERSs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

              return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtprices")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dataprices()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtprices");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_PRICESs result = null;
            List<Oracle.dATASET_PRICESs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_PRICESs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_PRICESs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

              return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
            
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtpricesG")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datapricesG()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtpricesG");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_PRICESs result = null;
            List<Oracle.dATASET_PRICESs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_PRICESs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_PRICESs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

            return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });

           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtpricesL")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datapricesL()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtpricesL");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            dATASET_PRICESs result = null;
            List<Oracle.dATASET_PRICESs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.dATASET_PRICESs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.dATASET_PRICESs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

             return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });

           
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtclients/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dataclient(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtclients/"+s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            eSCROW_CLIENTSs result = null;
            List<Oracle.eSCROW_CLIENTSs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.eSCROW_CLIENTSs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.eSCROW_CLIENTSs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

             return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtclientsA/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dataclientA(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtclientsA/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            eSCROW_CLIENTSs result = null;
            List<Oracle.eSCROW_CLIENTSs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.eSCROW_CLIENTSs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.eSCROW_CLIENTSs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
                {
                    Content = new StringContent(validate, Encoding.UTF8, "application/xml")
                };
            });

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtport/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dataport(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtport/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            portfolioes result = null;
            List<Oracle.portfolioes> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.portfolioes));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.portfolioes)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("issuers")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> saam2()
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/issuelist");
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
           return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
                {
                    Content = new StringContent(validate, Encoding.UTF8, "application/xml")
                };
            });

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtnumber/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> dataclientn(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnumber/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return await System.Threading.Tasks.Task.Run(() =>
            {

               return validate;
            });
            
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtnames/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> dataclientnn(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnames/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return await System.Threading.Tasks.Task.Run(() =>
            {

             return validate;
            });
            
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtcprice/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> dataclientnnn(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtcprice/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return await System.Threading.Tasks.Task.Run(() =>
            {

            return validate;
            });
            
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("corporate/{m}/{y}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> corporate(string m,string y)
        {
            DateTime date = Convert.ToDateTime("1" + m + y);
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            //a.counter.Replace(".", "").Replace(",","").Replace("&", "").Replace(" ", "")==s && 
            var p = BackC.CorporateActions.ToList().Where(a=>a.date>=firstDayOfMonth && a.date<=lastDayOfMonth.AddDays(1)).OrderByDescending(a=>a.date);
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return p;
            });

        }
       
        public string names(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnames/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            

                return validate;
           

        }
        public string number(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnumber/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return validate;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtid/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> idnumber2(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/did/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return await System.Threading.Tasks.Task.Run(() =>
            {

               return validate;
            });
           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dsub/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> activesub(string s)
        {
            var p = from v in BackC.Subscribers
                    where v.PhoneNumber==s
                    select new {v.subss,v.runnS };
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return p;
            });

        }
        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("dmark/{s}/{t}/{m}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> message(string s,string t,string m)
        {
            string res = "";
            var c = 0;
            try
            {
                c = BackC.Marks.ToList().Where(a => a.atpcsd == s && a.marktype == t && a.refID == m).Count();

            }
            catch (Exception)
            {

                c = 0;
            }

            if (c <=0)
            {
                Mark my = new BACKEND.Mark();
                my.atpcsd = s;
                my.datemarked = DateTime.Now;
                my.datemarkedtoexpire= DateTime.Now.AddDays(5);
                my.marktype = t;
                my.refID = m;
                BackC.Marks.Add(my);
                BackC.SaveChanges();
                res = "Message marked";
            }
            else{
                res = "Message already marked";
            }
            return await System.Threading.Tasks.Task.Run(() =>
             {

                return res;
            });
        }


        public string idnumber(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/did/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            return validate;
        }

        public string emails(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/demail/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            if (validate==null)
            {
                validate = "None";
            }
            return validate;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("demail/{s}")]
        [System.Web.Http.AllowAnonymous]
        public string emails22(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/demail/" + s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            if (validate == null)
            {
                validate = "None";
            }
            return validate;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtstmt/{s}/{dt}/{dt2}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> datastmt(string s,string dt,string dt2)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtstmt/"+s+"/"+dt+"/"+dt2);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            string valid = "";
            eSCROW_MINI_STMTs result = null;
            List<Oracle.eSCROW_MINI_STMTs> nn = null;
            //List<Oracle.User> dataList = JsonConvert.DeserializeObject<List<Oracle.User>>(validate);
            XmlSerializer serializer = new XmlSerializer(typeof(Oracle.eSCROW_MINI_STMTs));
            using (TextReader reader = new StringReader(validate))
            {
                result = (Oracle.eSCROW_MINI_STMTs)serializer.Deserialize(reader);
                //nn.Add(new Oracle.users { id = result.id, name = result.name, profession = result.profession });
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

               return new HttpResponseMessage()
            {
                Content = new StringContent(validate, Encoding.UTF8, "application/xml")
            };
            });
           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dtrade/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dtrade(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtrade/"+s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
          
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
                {
                    Content = new StringContent(validate, Encoding.UTF8, "application/xml")
                };
            });

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dalert/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> dalert(string s)
        {
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dalert/"+s);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
                {
                    Content = new StringContent(validate, Encoding.UTF8, "application/xml")
                };
            });

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("dinfo")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> dinfo()
        {
            var p = BackC.GeneralMessages.ToList();
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return p;
            });

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("drem/{s}/{f}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> dinfos(string s,string f)
        {
            var p = from v in BackC.Marks
                    where v.atpcsd == s && v.marktype == f && v.datemarkedtoexpire<=DateTime.Now
                    select new {v.refID };
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return p;
            });

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Sub/{mem}/{toAdd}/{toRemove}")]
        public async Task<string> SubscribeW(string mem, string toAdd, string toRemove)
        {
            string res = "";
            try
            {
                int prodid = 0;
                int memid = 0;
                //get mem id
                var e = BackC.Subscribers.ToList().Where(a => a.PhoneNumber.ToLower().Replace(" ", "") == mem.ToLower().Replace(" ", ""));
                foreach (var z in e)
                {
                    memid = z.Id;
                }
                //get [product id
                var f = BackC.Products.ToList().Where(a => a.Name.ToLower().Replace(" ", "") == toAdd.ToLower().Replace(" ", ""));
                foreach (var z in f)
                {
                    prodid = z.IDENTITY;
                }

                //check combination

                var chk = 0;
                try
                {
                    chk = BackC.SubscriberProducts.ToList().Where(a => a.ProductId == prodid && a.SubscriberId == memid).Count();

                }
                catch (Exception)
                {

                    chk = 0;
                }
                if (chk == 0)
                {
                    double six = 0;
                    try
                    {
                        six = Convert.ToDouble(number(mem).Substring(0,3));

                    }
                    catch (Exception)
                    {

                        six = 0;
                    }
                    var p = BackC.Operators.ToList().Where(a=>a.Prefix==six);
                    string tests = "";
                    foreach (var z in p)
                    {
                        tests = z.Operator1;
                    }
             

                    //insert into Subscriber Products
                    BACKEND.SubscriberProduct my = new BACKEND.SubscriberProduct();
                    my.ProductId = prodid;
                    my.SubscriberId = memid;
                    my.Date = DateTime.Now;
                    if (tests == "Airtel")
                    {
                        my.Response = DirectDebitForOBOPAY_12SEP_Test("MAkibadrUserName", "MAkiba123#Password", "+254" + number(mem), 1, "254731214472");

                    }
                    else if (tests == "Safaricom")
                    {
                        var client = new RestClient("http://localhost:58888/EscrowService/rest/UserService/simulate/272007/CustomerPayBillOnline/1/254" + number(mem)+"/1");
                        var request = new RestRequest("", Method.POST);
                        IRestResponse response = client.Execute(request);
                        string validate = response.Content;
                        validate = validate.Replace(@"""", "");
                        my.Response = validate;
                    }
                    else
                    {
                        my.Response = "Failed";
                    }
                        BackC.SubscriberProducts.Add(my);
                    await BackC.SaveChangesAsync();
                    res = "Product was added successfully";

                  
                }
                else
                {
                    res = "Already subscribed";
                }
            }
            catch (Exception)
            {

                res = "Failed to add product";
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Register/{username}/{password}/{membernumber}/{regid}")]
        public async Task<string> Register(string username, string password,string membernumber,string regid)
        {//check if email exsists
            membernumber = membernumber.Replace("-", "*");
            regid = regid.Replace("-", "/");
            string unm = membernumber;
            string s = membernumber;
            string add = "";
            s = s.Replace("-", "*");
            if (s.Length < 13)
            {
                int diff = 13 - s.Length;
                for (int x = 0; x < diff; x++)
                {
                    add = add + "0";
                }
                s = add + s;
            }
            membernumber = s;
            //check cdsnumber
            string res = "";
            int myy = 0;
            int nn = 0;
            int ee = 0;
            string sms = "";
            string allowedChars = "";
           allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 4; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            try
            {
                myy = BackC.Subscribers.ToList().Where(a => a.PhoneNumber == membernumber).Count();

            }
            catch (Exception)
            {

                myy = 0;
            }
            string nail = emails(membernumber);


             if (myy > 0)
                {
                    res = "Duplicate Client numbers are not allowed";
                }
                else if (emails(membernumber)=="None" || emails(membernumber) ==null)
                {
                    res = "Email does not exsist";
                }
                else if (regid.Replace("-", "").Replace("/", "") != idnumber(membernumber).Replace("-", "").Replace("/", ""))
                {
                    res = "ID does not exsist";
                }
                else if (myy==0 && nail!=null && nail!="None")
                {//pad zeros to length
                try
                {
                    var resp = BackC.ParameterCharges.ToList().Where(a => a.Parametername.ToLower() == ("REGISTER").ToLower());
                    decimal cm = 0;
                    foreach (var z in resp)
                    {
                        cm = z.charge;
                    }
                    regid = idnumber(membernumber);
                    if (regid=="")
                    {
                        regid = "none";
                    }
                    BACKEND.Subscriber my = new BACKEND.Subscriber();
                    my.Username = regid;
                    my.Password = password;
                    my.Email = emails(membernumber);
                    my.PhoneNumber = membernumber;
                    my.DateTime = DateTime.Now;
                    my.RegDate = my.DateTime;
                    my.Active = true;
                    my.runnS = "NPAID";
                    my.subss =my.DateTime;
                    my.Code = passwordString;
                    double six = 0;
                    try
                    {
                        six = Convert.ToDouble(number(membernumber).Substring(0, 3));

                    }
                    catch (Exception)
                    {

                        six = 0;
                    }
                    var p = BackC.Operators.ToList().Where(a => a.Prefix == six);
                    string tests = "";
                    foreach (var z in p)
                    {
                        tests = z.Operator1;
                    }

                    if (tests == "Airtel")
                    {
                        my.Status = DirectDebitForOBOPAY_12SEP_Test("MAkibadrUserName", "MAkiba123#Password", "254" + number(membernumber), cm, "254731214472");
                        if (my.Status.Contains("Success"))
                        {
                            my.Status = "SUCCESS";
                            my.runnS = "PAID";
                            my.subss = my.DateTime.AddYears(1);
                         }
                        my.CheckoutRequestID ="AR";
                        try
                        {
                            SafariTran mw = new SafariTran();
                            mw.MerchantRequestID = "Airtel";
                            mw.CheckoutRequestID= "+254" + number(membernumber);
                            mw.CustomerMessage ="Airtel";
                            mw.ResponseCode = "Airtel";
                            mw.ResponseDescription = "Airtel";
                            mw.CallBack = my.Status;
                            BackC.SafariTrans.Add(mw);
                            //await BackC.SaveChangesAsync();

                            my.CheckoutRequestID = mw.CheckoutRequestID;
                        }
                        catch (Exception)
                        {


                        }
                    }
                    else if (tests == "Safaricom")
                    {

                                       
                        var ne = LipaM("254"+number(my.PhoneNumber).TrimStart('0'), my.PhoneNumber,"1", my.DateTime.ToString("yyyymmddHHmmss"));
                        //check ne contains success
                        if (ne != null)
                        {
                         my.Status = "PENDING";
                        my.runnS = "NPAID";
                            try
                            {
                                dynamic o = JsonConvert.DeserializeObject(ne.ToString());
                                SafariTran mw = new SafariTran();
                                mw.MerchantRequestID = o.MerchantRequestID;
                                mw.CheckoutRequestID = o.CheckoutRequestID;
                                my.CheckoutRequestID = o.CheckoutRequestID;
                                mw.CustomerMessage = o.CustomerMessage;
                                mw.ResponseCode = o.ResponseCode;
                                mw.ResponseDescription = o.ResponseDescription;
                                BackC.SafariTrans.Add(mw);
                                //await BackC.SaveChangesAsync();

                             
                            }
                            catch (Exception)
                            {

                              
                            }
                        }

                    }
                    else
                    {
                        my.Status = "Failed";
                    }
                    my.chessmaster = "CHANGED";
                   
                    BackC.Subscribers.Add(my);
                    await BackC.SaveChangesAsync();
                    string refz = "Thank you for registering CDS account " + unm  + " on our mobile application.";

                    try
                    {
                        SendMail2(emails(my.PhoneNumber), refz, "CDSC MOBILE APP::Registration", s);
                        res = "Successfully registered";
                    }
                    catch (Exception f)
                    {

                        writetofile(f.Message);
                    }
                   
                }           
                catch (Exception g)
                {

                    res = g.ToString();
                    writetofile(res);
                }
                    //subscribe

                 }




            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Airtel/{a}/{b}")]
        [System.Web.Http.AllowAnonymous]
        public  dynamic Airrs(string a, string b)
        {
            return DirectDebitForOBOPAY_12SEP_Test("MAkibadrUserName","MAkiba123#Password",a, Convert.ToDecimal(b), "254731214472");

           
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Lipa/{a}/{b}/{c}/{d}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> Lipas(String a,String b,String c,String d)
        {
            var client = new RestClient("http://localhost:58888/EscrowService/rest/UserService/lipa/"+a+"/"+b+"/"+c+"/"+d);
            var request = new RestRequest("", Method.POST);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
                {
                    Content = new StringContent(validate, Encoding.UTF8, "application/xml")
                };
            });

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Lipareq/{a}/{b}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<HttpResponseMessage> Lipasr(string a, string b)
        {
            var client = new RestClient("http://localhost:58888/EscrowService/rest/UserService/lireq/" + a + "/" + b);
            var request = new RestRequest("", Method.POST);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return new HttpResponseMessage()
                {
                    Content = new StringContent(validate, Encoding.UTF8, "application/xml")
                };
            });

        }


        public dynamic LipaM(String a, String b, String c, String d)
        {
            var client = new RestClient("http://localhost:58888/EscrowService/rest/UserService/lipa/" + a + "/" + b + "/" + c + "/" + d);
            var request = new RestRequest("", Method.POST);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            return validate;

        }
        
        public dynamic LipasrM(string a, string b)
        {
            var client = new RestClient("http://localhost:58888/EscrowService/rest/UserService/lireq/" + a + "/" + b);
            var request = new RestRequest("", Method.POST);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            return validate;

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("activate/{u}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> nemess(string u)
        {
            u = u.Replace("-", "*");
            string res = "";
            int my = 0;
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber == u
                    select v;
            foreach (var k in c)
            {
                my = k.Id;
            }  
                try
                {
                    BACKEND.Subscriber nw = BackC.Subscribers.Find(my);
                    var resp = BackC.ParameterCharges.ToList().Take(1);
                    decimal cm = 0;
                     nw.DateTime = DateTime.Now;
                    //nw.CheckoutRequestID = "UPDATE";
                    foreach (var z in resp)
                    {
                        cm = z.charge;
                    }
                    double six = 0;
                    try
                    {
                        six = Convert.ToDouble(number(u).Substring(0, 3));

                    }
                    catch (Exception)
                    {

                        six = 0;
                    }
                    var p = BackC.Operators.ToList().Where(a => a.Prefix == six);
                    string tests = "";
                    foreach (var z in p)
                    {
                        tests = z.Operator1;
                    }
                    string validate = "";
                    res = "Failed";
                    if (tests == "Airtel")
                    {
                        nw.Status = DirectDebitForOBOPAY_12SEP_Test("MAkibadrUserName", "MAkiba123#Password", "254" + number(u), cm, "254731214472");
                        if (nw.Status.Contains("Success"))
                        {
                            res = "Success";
                            nw.Status = "SUCCESS";
                            nw.runnS = "PAID";
                            nw.subss = DateTime.Now.AddYears(1);
                            res = "Subcsription renewal successful.";
                        }
                        try
                        {
                            SafariTran mw = new SafariTran();
                            mw.MerchantRequestID = "Airtel";
                            mw.CheckoutRequestID = "+254" + number(nw.PhoneNumber);
                            mw.CustomerMessage = "Airtel";
                            mw.ResponseCode = "Airtel";
                            mw.ResponseDescription = "Airtel";
                            nw.CheckoutRequestID = "UPDATE";
                            mw.CallBack = nw.Status;
                            BackC.SafariTrans.AddOrUpdate(mw);
                            BackC.SaveChanges();

                          
                        }
                        catch (Exception f)
                        {

                        res = f.Message;
                    }
                    }
                    else if (tests == "Safaricom")
                    {
                        var ne = LipaM("254" + number(nw.PhoneNumber).TrimStart('0'),nw.PhoneNumber,"1", nw.DateTime.ToString("yyyymmddHHmmss"));
                        //check ne contains success
                        nw.Status = "PENDING";
                        nw.runnS = "NPAID";
                        //res = "Thank you very much ,please approve the payment request sent to your mobile";
                        res = "Do you want to pay KES 1. to CDSC CDSC MOBILE APP SUBSCRIPTION";
                        try
                        {
                            dynamic o = JsonConvert.DeserializeObject(ne.ToString());
                            SafariTran mw = new SafariTran();
                            mw.MerchantRequestID = o.MerchantRequestID;
                            mw.CheckoutRequestID = o.CheckoutRequestID;
                           nw.CheckoutRequestID = o.CheckoutRequestID;
                            mw.CustomerMessage = o.CustomerMessage;
                            mw.ResponseCode = o.ResponseCode;
                            mw.ResponseDescription = o.ResponseDescription;
                            BackC.SafariTrans.AddOrUpdate(mw);
                            BackC.SaveChanges();

                            
                        }
                        catch (Exception f)
                        {
                            res = f.Message;
                        }
                    }
                    else
                    {
                        nw.Status = "Failed";
                    }
                    
                    BackC.Subscribers.AddOrUpdate(nw);
                BackC.SaveChanges();
                }

                catch (Exception f)
                {

                    res = f.Message;
                }

            

            
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
            
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Products")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> nemes()
        {
           dynamic products = null;
            try
            {
              products = (from m in BackC.Products
                          select new { m.Name }).ToList();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                
                return await System.Threading.Tasks.Task.Run(() =>
                {

                    return (dynamic)raise.ToString();
                });
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return products;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Subscribed/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> neme(string s){
            int my = 0;
            var sx = BackC.Subscribers.ToList().Where(a=>a.PhoneNumber==s);
            foreach (var e in sx)
            {
                my = e.Id;
            }
            var products = from m in BackC.SubscriberProducts
                           join v in BackC.Products on m.ProductId equals v.IDENTITY
                           where m.SubscriberId == my
                           select new { v.Name };
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return products;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Change/{s}/{p}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LoginMemPut(string s, string p)
        {
            s = s.Replace("-", "*");
            string res = "";
            int my = 0;
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber == s
                    select v;
            foreach (var k in c)
            {
                my = k.Id;

   try
            {
                BACKEND.Subscriber nw = BackC.Subscribers.Find(my);
                nw.Password = p;
                nw.chessmaster = "CHANGED";
                BackC.Subscribers.AddOrUpdate(nw);

                res = "Password was saved successfully";
            }

            catch (Exception f)
            {

                res = f.Message;
            }
                
            }
         
            await BackC.SaveChangesAsync();
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        //Profiles update
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Profile/{username}/{email}/{csdnumber}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LoginMemPutUU(string username, string email,string csdnumber)
        {
            csdnumber = csdnumber.Replace("-", "*");
            string res = "";
            int my = 0;
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber == csdnumber
                    select v;

            //ccheck if username or email exsists
            var nn = BackC.Subscribers.ToList().Where(a => a.Username == username && a.PhoneNumber!=csdnumber).Count();
            var ss= BackC.Subscribers.ToList().Where(a => a.Email== email && a.PhoneNumber != csdnumber).Count();
            if (nn > 0)
            {
                res = "Username is already taken";
            }
            else if (ss > 0)
            {
                res = "Email is already taken";
            }
            else
            {


                foreach (var k in c)
                {
                    my = k.Id;

                    try
                    {
                        BACKEND.Subscriber nw = BackC.Subscribers.Find(my);
                        nw.Username = username;
                        nw.Email = email;
                        BackC.Subscribers.AddOrUpdate(nw);

                        res = "Password was saved successfully";
                    }

                    catch (Exception f)
                    {

                        res = f.Message;
                    }

                }

               await BackC.SaveChangesAsync();
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            }); ;
        }
        //Profile Accept
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("ProfileAccept/{csdnumber}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> LoginMemPutUUA(string csdnumber)
        {
            csdnumber = csdnumber.Replace("-", "*");
            string res = "";
            int my = 0;
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber == csdnumber
                    let Name=v.Username
                    let Mail=v.Email
                    select new { Name,Mail };
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return c;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Ammend/{email}/{mobile}/{postal}/{cds}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> Ammend(string email,string mobile,string postal,string cds)
        {
            postal = postal.Replace("%20"," ");
            postal = postal.Replace("-", "/");
            string res = "Successfully added";
            try
            {
                Ammend my = new BACKEND.Ammend();
                my.Email = email;
                my.Mobile = mobile;
                my.Postal = postal;
                my.existingcds = cds;
                my.capture = "CAPTURE";
                BackC.Ammends.AddOrUpdate(my);
                await BackC.SaveChangesAsync();
            }
            catch (Exception)
            {

                res = "Failed to save update";
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("CDSAcc/{postal}/{Surname}/{Forenames}/{Title}/{IDNoPP}/{DateOfBirth}/{Nationality}/{Mobile}/{Email}/{Bank}/{Account_Number}/{cdsaccount}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> cdsacc(string postal, string Surname,string Forenames,string Title,string IDNoPP,string DateOfBirth, string Nationality,string Mobile, string Email,string Bank,string Account_Number, string cdsaccount)
        { 
         string res = "Successfully createdaccount , move onto to step 2 to add documents";
            postal = postal.Replace("%20", " ");
            Bank = Bank.Replace("%20", " ");
            int max = 0;
            try
            {
                max = BackC.CDSAccounts.ToList().Max(a => a.CDSAccountID);
            }
            catch (Exception)
            {

                max = 0;
            }
            try
            {
                CDSAccount my = new BACKEND.CDSAccount();
                my.postal = postal;
                my.Surname = Surname;
                my.Forenames = Forenames;
                my.Title = Title;
                my.IDNoPP = IDNoPP;
                my.DateOfBirth = Convert.ToDateTime(DateOfBirth);
                my.Nationality = Nationality;
                my.Mobile = Mobile;
                my.Email =Email;
                my.Bank = Bank;
                my.Account_Number = Account_Number;
                my.cdsaccount1 = cdsaccount;
                BackC.CDSAccounts.AddOrUpdate(my);
                await BackC.SaveChangesAsync();
            }
            catch (Exception)
            {

                res = "Failed to save account";
            }
            
            if (res=="Successfully createdaccount , move onto to step 2 to add documents")
            {
                res = res + ":" + (max + 1).ToString();
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AddDocument/{doc_generated}/{ContentType}/{Data}/{CDSAccountID}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> GetNotifications()
        {
            var cc = BackC.GeneralMessages.ToList().Where(a=>a.expiryDate<=DateTime.Now);

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return cc;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AddDocument/{doc_generated}/{ContentType}/{Data}/{CDSAccountID}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<dynamic> AccDD(string doc_generated,string ContentType,byte[] Data, string CDSAccountID)
        {
         
          string res = "Successfully added document";
            //try
            //{
            //    Accounts_Document my = new BACKEND.Accounts_Documents();
            //    my.doc_generated = doc_generated;
            //    my.ContentType = ContentType;
            //    my.Data = Data;
            //    my.CDSAccountID = Convert.ToInt32(CDSAccountID);
            //    BackC.Accounts_Documents.AddOrUpdate(my);
            //    await BackC.SaveChangesAsync();
            //}
            //catch (Exception)
            //{

            //    res = "Failed to save update";
            //}

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("LoginMem/{s}/{p}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LoginMem(string s, string p)
        {
            string add = "";
            s = s.Replace("-", "*");
            if (s.Length < 13)
            {
                int diff = 13 - s.Length;
                for(int x = 0; x < diff; x++)
                {
                    add = add + "0";
                }
                s = add + s;
            }
            string res = "0";
            string mail = "";
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber == s && v.Password == p &&  v.Active==true
                    select v;
            foreach (var k in c)
            {
                res = k.PhoneNumber;
                mail = k.Email;
                res = "1";
            }
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 4; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            //Generate OTP PIN
            foreach (var k in c)
            {
                k.Code = passwordString;
            }
            var z = BackC.Subscribers.Find(emmz(s));
            z.Code= passwordString;
            BackC.Subscribers.AddOrUpdate(z);
            await BackC.SaveChangesAsync();
            //email the code
            string refz = "Your one -time 4 digit login code " + passwordString;
            if (mail!="")
            {
                try
                {
                    SendMail2(mail, refz, "CDSC MOBILE APP::Login code", s);

                }
                catch (Exception f)
                {
                    writetofile(f.Message);
                }
                res = "1";
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        public string emm(string z)
        {
            string sav = "";
            var tt = BackC.Subscribers.ToList().Where(a=>a.PhoneNumber.Trim()==z.Trim());
            foreach(var r in tt)
            {
                sav = r.Email;
            }
            return sav;
        }

        public int emmz(string z)
        {
            int sav = 0;
            var tt = BackC.Subscribers.ToList().Where(a => a.PhoneNumber == z);
            foreach (var r in tt)
            {
                sav = r.Id;
            }
            return sav;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Link/{s}/{m}/{t}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LinkAccounts(string s,string m,string t)
        {
            var gv = BackC.Subscribers.ToList().Where(a=>a.PhoneNumber==s);
            int hv = 0;
            foreach (var q in gv){
                hv = q.Id;
            }
            if (isValid(m,t))
            {
                //ding linked accounts
                LinkedAccount nm = new LinkedAccount();
                nm.CSD = m;
                nm.mobile = t;
                nm.SubscriberRef = 1;
                nm.SubscriberID = hv;
                BackC.LinkedAccounts.Add(nm);
               await BackC.SaveChangesAsync();
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return "success";
            });
            
        }

        public bool isValid(string m,string t)
        {
            return true;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("OtpV/{s}/{m}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> ValidOtp(string s,string m)
        { string response = "0";
            try
            {
               
                string add = "";
                s = s.Replace("-", "*");
                if (s.Length < 13)
                {
                    int diff = 13 - s.Length;
                    for (int x = 0; x < diff; x++)
                    {
                        add = add + "0";
                    }
                    s = add + s;
                }
                var c = BackC.Subscribers.ToList().Where(a => a.PhoneNumber == s && a.Code == m).Count();

                if (c > 0)
                {
                    response = "1";
                }
                else
                {
                    response = "0";
                }
            }
            catch (Exception)
            {

                response = "0";
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return response;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("ResetV/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> ValidP(string s)
        {
            string response = "0";
            try
            {

                string add = "";
                s = s.Replace("-", "*");
                if (s.Length < 13)
                {
                    int diff = 13 - s.Length;
                    for (int x = 0; x < diff; x++)
                    {
                        add = add + "0";
                    }
                    s = add + s;
                }
                var c = BackC.Subscribers.ToList().Where(a => a.PhoneNumber == s);

                foreach (var p in c)
                {
                    response = p.chessmaster;
                }
            }
            catch (Exception)
            {

                response = "CHANGED";
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return response;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("SubscrV/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> SalidP(string s)
        {
            string response = "CHANGED";
            var dt = DateTime.Now.ToString("yyyymmdd");
            try
            {

                string add = "";
                s = s.Replace("-", "*");
                if (s.Length < 13)
                {
                    int diff = 13 - s.Length;
                    for (int x = 0; x < diff; x++)
                    {
                        add = add + "0";
                    }
                    s = add + s;
                }
                var c = BackC.Subscribers.ToList().Where(a => a.PhoneNumber == s);

                foreach (var p in c)
                {
                    if (dt==Convert.ToDateTime(p.subss).ToString("yyyymmdd"))
                    {
                        response = "SUBSCRIBE";
                    }
                }
            }
            catch (Exception)
            {

                response = "CHANGED";
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return response;
            });
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Otp/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string>  ResendOtp(string s)
        {
            s = s.Replace("-", "*");
            string add = "";
            s = s.Replace("-", "*");
            if (s.Length < 13)
            {
                int diff = 13 - s.Length;
                for (int x = 0; x < diff; x++)
                {
                    add = add + "0";
                }
                s = add + s;
            }
            string res = "";
            string mail = "";
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber==s
                    select v;
           
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 4; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            //Generate OTP PIN
            foreach (var k in c)
            {
                k.Code = passwordString;
                mail = k.Email;
            }
            await BackC.SaveChangesAsync();
            //email the code
            string refz = "Your one -time 4 digit login code " + passwordString;
            SendMail2(mail, refz, "CDSC MOBILE APP::Login code", s);
            res = "1";
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //[System.Web.Http.HttpGet]
        //[System.Web.Http.Route("Banks")]
        //[System.Web.Http.AllowAnonymous]
        //public async Task<dynamic> Bank()
        //{
        //    var res = BackC.para_bank.ToList().Select(a=>a.bank_name);
        //    return await System.Threading.Tasks.Task.Run(() =>
        //    {

        //        return res;
        //    });
        //}
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //[System.Web.Http.HttpGet]
        //[System.Web.Http.Route("Branch/{s}")]
        //[System.Web.Http.AllowAnonymous]
        //public async Task<dynamic> Branch(string s)
        //{
        //    var res = BackC.para_bank.ToList().Where(a=>a.bank_name.ToLower().Replace(" ","")==s.ToLower().Replace(" ", ""));
        //    string code = "";
        //    foreach (var d in res)
        //    {
        //        code = d.bank;
        //    }
        //    var p = BackC.para_branch.ToList().Where(a=>a.bank==code && a.branch_name!="INVALID").Select(a=>a.branch_name);
        //    return await System.Threading.Tasks.Task.Run(() =>
        //    {

        //        return p;
        //    });
        //}
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("AppUsage/{a}/{m}/{l}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> AppUsage(string a, string m,string l)
        {
            a = a.Replace("-"," ");
            string res = "Done";
            BACKEND.AppUsage my = new BACKEND.AppUsage();
            my.UserId = m;
            my.Activity = a;
            my.Date = DateTime.Now;
            my.Location = l.Replace("$", ",");
            BackC.AppUsages.Add(my);
            await BackC.SaveChangesAsync();
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Forg/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async  Task<string> LoginMemPutL(string s)
        {
            s = s.Replace("-", "*");
            string add = "";
            s = s.Replace("-", "*");
            if (s.Length < 13)
            {
                int diff = 13 - s.Length;
                for (int x = 0; x < diff; x++)
                {
                    add = add + "0";
                }
                s = add + s;
            }
            string res = "";
            int my = 0;
            string mailm = "";
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber.ToLower() == s.ToLower()
                    select v;
            foreach (var k in c)
            {
                my = k.Id;
                mailm = k.Email;
            }
            string sms = "";
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 8; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
         
            try
            {
             var Password = ComputeHash(passwordString, new SHA256CryptoServiceProvider());
              var chessmaster = "RESET";
              //  BackC.Subscribers.AddOrUpdate(nw);

                int myY = BackC.Database.ExecuteSqlCommand("Update Subscriber set chessmaster='RESET',Password='" + Password + "' where Id='"+ my +"' ");
               //await BackC.SaveChangesAsync();
                res = "Password was reset successfully "+ passwordString;
                //send sms
                try
                {
                    SendMail2(mailm, res, "CDSC MOBILE Password Reset",s);

                }
                catch (Exception f)
                {

                    res = f.Message;
                    writetofile(res);
                }

                res = "Password was reset successfully ";
            }
            catch (Exception f)
            {

                res = f.Message+mailm+my.ToString();
                writetofile(res);
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }
        //load list from json
        
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("makibah/{s}")]
        [AllowAnonymous]
        public async Task<dynamic> conTT(string s)
        {
         
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba"].ConnectionString;
           var q1 = "SELECT a.COMPANY, a.CDS_Number, ISNULL(SUM(a.SHARES),0) as 'Shares' FROM TRANS a JOIN Account_Creation b on a.CDS_Number=b.CDSC_Number where b.Identification= '"+idnumber(s)+"' GROUP BY a.Company, a.CDS_Number";

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = q1;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<CC>();
            while (reader.Read())
            {
                var accountDetails = new CC
                {

                    Company = reader.GetValue(0).ToString(),
                    CDS_Number = reader.GetValue(1).ToString(),
                    Shares = Math.Round(Convert.ToDecimal(reader.GetValue(2)), 4).ToString()

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return await System.Threading.Tasks.Task.Run(() =>
            {

                return accDetails;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Deactivate/{s}")]
        [System.Web.Http.AllowAnonymous]
        public async Task<string> LoginMemPutLL(string s)
        {
            s = s.Replace("-", "*");
            string res = "";
            int my = 0;
            string mailm = "";
            var c = from v in BackC.Subscribers
                    where v.PhoneNumber.ToLower() == s.ToLower()
                    select v;
            string names = "";
            foreach (var k in c)
            {
                my = k.Id;
                mailm = k.Email;
                names = k.Username;
            }
            string sms = "";
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";

            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 14; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            try
            {
                Subscriber nw = BackC.Subscribers.Find(my);
                //nw.Active = false;
                BackC.Subscribers.Remove(nw);
                await BackC.SaveChangesAsync();
                res = "Dear "+ names + "\n\n Account number "+ s +" has been deactivated on CDSC MOBILE APP";
                //send sms
                try
                {
                    SendMail2(mailm, res, "CDSC Mobile Account Deactivation",s);

                }
                catch (Exception)
                {

                    
                }  }
            catch (Exception)
            {

                res = "Password was not saved";
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return res;
            });
        }

        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
        public void SendMail2(string id, string refz, string name,string mem)
        {
            //SendMail(email,refz,HttpUtility.UrlDecode(name));
            SmtpClient client = new SmtpClient();
            //            client.Port = 587;
            //            client.Host = "smtp.gmail.com";
            //            client.EnableSsl = true;
            //            ServicePointManager.ServerCertificateValidationCallback =
            //delegate (object s, X509Certificate certificate,
            //       X509Chain chain, SslPolicyErrors sslPolicyErrors)
            //{ return true; };
            client.Port = 25;
            client.Host = "192.168.5.4";

            client.Timeout = 100000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("MOBILEAPP@cdsckenya.com", "");
            //client.Timeout = 100000;
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //client.UseDefaultCredentials = false;
            //client.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");
            StreamReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory+"/HTML/new_member.html");
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$Fullnames$$", names(mem));
            myString = myString.Replace("$$cds$$", mem);
            myString = myString.Replace("$$Message$$", refz);
           MailMessage mm = new MailMessage("MOBILEAPP@cdsckenya.com", emm(mem), "CDSC MOBILE APP NOTIFICATION " + name, myString.ToString());
            mm.IsBodyHtml = true;
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(mm);
            reader.Dispose();
        }
        //payment methods
        public string DirectDebitForOBOPAY_12SEP_Test(string Username, string Password, string CustMobile, decimal Amount, string MerchantMSISDN)
        {
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslerror) => true;

                var request = HttpWebRequest.Create("https://41.223.58.41:8446/Service1.asmx");
                byte[] bytes = null;
                string strXmlInputData = string.Empty;

                strXmlInputData += "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'>";
                strXmlInputData += "<soapenv:Header/>";
                strXmlInputData += "<soapenv:Body>";
                strXmlInputData += "<tem:DirectDebitAPI>";
                strXmlInputData += "<tem:UserName>" + Username + "</tem:UserName>";
                strXmlInputData += "<tem:Password>" + Password + "</tem:Password>";
                strXmlInputData += "<tem:CustomerMobileNumber>" + CustMobile + "</tem:CustomerMobileNumber>";
                strXmlInputData += "<tem:Amount>" + Amount + "</tem:Amount>";
                strXmlInputData += "<tem:MerchantWalletMsisdn>" + MerchantMSISDN + "</tem:MerchantWalletMsisdn>";
                strXmlInputData += "</tem:DirectDebitAPI>";
                strXmlInputData += "</soapenv:Body>";
                strXmlInputData += "</soapenv:Envelope>";
                bytes = System.Text.Encoding.ASCII.GetBytes(strXmlInputData);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                using (WebResponse myWebResponse = request.GetResponse())
                {
                    using (Stream myResponseStream = myWebResponse.GetResponseStream())
                    {
                        using (StreamReader myStreamReader = new StreamReader(myResponseStream))
                        {
                            return myStreamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException er)
            {
                writetofile(er.ToString());
                return "XX";
            }
            catch (Exception ex)
            {
                writetofile(ex.ToString());
                return "XX";
            }
        }
        public void writetofile(String mssg)
        {
            string path = HttpContext.Current.Server.MapPath("api\\error" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt");
            System.IO.StreamWriter SW = default(System.IO.StreamWriter);
            SW = File.CreateText(path);
            SW.WriteLine(mssg.ToString());
            SW.Close();
        }
        //writing enquiries

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("ticketN/{ticket}/{type}")]
        [AllowAnonymous]
        public async Task<dynamic> NotificationDetails(string ticket, string type)
        {
            var urls4 = BackC.FeedBackTrans.ToList();
            if (type == "Inbox")
            {
                urls4 = (from s in BackC.FeedBackTrans
                         where s.ticketRef == ticket && s.IsClientNotification == true
                         select s).ToList();

            }
            else if (type == "Sent")
            {
                urls4 = (from s in BackC.FeedBackTrans
                         where s.ticketRef == ticket && s.IsAppNotification == true
                         select s).ToList();
            }


            return await System.Threading.Tasks.Task.Run(() =>
            {

                return urls4;
            });
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("tickets/{ticket}/{response}")]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        public async Task<string> TicketResponse(string ticket, string response)
        {
            int id = 0;
            var tick = BackC.tblFeedBackMains.Where(a => a.ticketRef == ticket);
            foreach (var c in tick)
            {
                id = c.tblFeedBackMainID;
            }
            if (tick.Count() > 0)
            {
                try
                {
                    FeedBackTran my = new FeedBackTran();
                    //Debit
                    my.ticketRef = ticket;
                    my.FeedID = id;
                    my.responsemessage = HttpUtility.UrlDecode(response);
                    my.IsClientNotification = false;
                    my.IsAppNotification = true;
                    my.datereceived = DateTime.Now;
                    my.UserID = 0;
                   BackC.FeedBackTrans.Add(my);
                    //Update table  
                    await BackC.SaveChangesAsync();
                    //alert operator in charge
                    var d = BackC.tblFeedBackMains.ToList().Where(a => a.ticketRef == ticket);
                    string myD = "";
                    foreach (var c in d)
                    {
                        myD = c.logon;
                    }
                    int n = Convert.ToInt32(myD);
                    var p = BackC.Users.ToList().Where(a => a.UserId == n);
                    foreach (var v in p)
                    {
                        SendMail3(v.Email, "Ticket No." + ticket + ":" + my.responsemessage, v.FirstName + " " + v.LastName, ticket);

                    }

                    return await System.Threading.Tasks.Task.Run(() =>
                    {

                        return "Success";
                    });
                   
                }
                catch (Exception)
                {
                    return await System.Threading.Tasks.Task.Run(() =>
                    {

                       return "Ticket Reference number not found";
                    });
                    
                }
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

              return "Ticket Reference number not found";
            });
            
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Acc/{broker}/{joint}/{taxexmpt}/{surname}/{name}/{company}/{telephone}/{fax}/{emails}/{nations}/{identif}/{ClientC}/{option}/{banks}/{branch}/{account}/{radtype}")]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        public async Task<string> BrAdd(string broker, string joint, string taxexmpt,string surname,string name,string company,string telephone,string fax,string emails,string nations,string identif,string ClientC,string option,string banks,string branch ,string account,string radtype)
        {
            string p = "Success";
            try
            {
                MobileCreationP my = new MobileCreationP();
                my.broker = Decrypt_QueryString(broker);
                my.joint = Decrypt_QueryString(joint);
                my.taxexmpt = Decrypt_QueryString(taxexmpt);
                my.surname = Decrypt_QueryString(surname);
            my.name = Decrypt_QueryString(name);
                my.company = Decrypt_QueryString(company);
                my.telephone = Decrypt_QueryString(telephone);
                my.fax = Decrypt_QueryString(fax);
                my.emails = Decrypt_QueryString(emails);
                my.nations = Decrypt_QueryString(nations);
                my.identif = identif;
                my.ClientC = Decrypt_QueryString(ClientC);
                my.option = Decrypt_QueryString(option);
                my.banks = Decrypt_QueryString(banks);
                my.branch = Decrypt_QueryString(branch);
                my.account = Decrypt_QueryString(account);
                my.radtype = Decrypt_QueryString(radtype);
                my.refID = 0;
                my.status = "PENDING";
                my.DateCreated = DateTime.Now;
                BackC.MobileCreationPs.Add(my);
                BackC.SaveChanges();

            }
            catch (Exception)
            {

                throw;
            }
            return await System.Threading.Tasks.Task.Run(() =>
            {

                return p;
            });

        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("ticketAdd/{feedback}/{mobile}")]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        public async Task<string> TicketNotification(string feedback,string mobile)
        {
            int? myD = 0;
            string sms = "";
            string eml = "";
            var c = BackC.Subscribers.ToList();
            var client = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnumber/" + mobile);
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            foreach (var p in c)
            {
                eml = p.Email;
            }
            try
            {
                try
                {
                    myD = BackC.tblFeedBackMains.Max(a => a.tblFeedBackMainID);
                }
                catch (Exception)
                {

                    myD = 0;
                }

                var client2 = new RestClient("http://192.168.4.166:8080/EscrowService/rest/UserService/dtnames/" + mobile);
                var request2 = new RestRequest("", Method.GET);
                IRestResponse response2 = client2.Execute(request2);
                string validate2 = response2.Content;
                validate2 = validate2.Replace(@"""", "");
                tblFeedBackMain my = new tblFeedBackMain();
                //Debit
               my.Feedback = HttpUtility.UrlDecode(feedback);
                my.Email = eml;
                my.mobile = validate;
                my.name = validate2;
                my.logon = "0";
                my.ticketstatus = "INCOMING";
                my.datereceived = DateTime.Now;
                my.ticketRef = "CDSC0000" + (myD + 1).ToString();
                BackC.tblFeedBackMains.Add(my);
                //Update table  
                await BackC.SaveChangesAsync();
                string refz = "Thank you for using our CDSC MOBILE APP your ticket Number is " + my.ticketRef;
                //sms = SendSMS(refz, mobile);
                SendMail4(eml, refz, HttpUtility.UrlDecode(my.name));
                return await System.Threading.Tasks.Task.Run(() =>
                {

                    return  "Success:" + my.ticketRef;
                });
                
            }
            catch (Exception e)
            {
                return await System.Threading.Tasks.Task.Run(() =>
                {

                   return e.ToString();
                });
                
            }
        }

        public void SendMail3(string id, string refz, string name, string ticket)
        {
            //SendMail(email,refz,HttpUtility.UrlDecode(name));
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
delegate (object s, X509Certificate certificate,
       X509Chain chain, SslPolicyErrors sslPolicyErrors)
{ return true; };
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");

            MailMessage mm = new MailMessage("edocwebapp@gmail.com", id, "CDSC MOBILE Ticket Updates: " + ticket + " " + name, refz);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(mm);

        }
        public  void SendMail4(string id, string refz, string name)
        {
            //SendMail(email,refz,HttpUtility.UrlDecode(name));
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
delegate (object s, X509Certificate certificate,
       X509Chain chain, SslPolicyErrors sslPolicyErrors)
{ return true; };
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");

            MailMessage mm = new MailMessage("edocwebapp@gmail.com", id, "CDSC MOBILE APP NOTIFICATION " + name, refz);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(mm);

        }

        public string Decrypt_QueryString(string str)
        {
           
            return str.Replace("-", " ");
        }

    }
}
