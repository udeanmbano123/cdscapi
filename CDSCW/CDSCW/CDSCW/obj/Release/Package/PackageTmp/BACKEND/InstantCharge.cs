//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CDSCW.BACKEND
{
    using System;
    using System.Collections.Generic;
    
    public partial class InstantCharge
    {
        public int InstantChargeID { get; set; }
        public string Charge { get; set; }
        public string ChargeCode { get; set; }
        public string ChargeDescription { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime Date { get; set; }
    }
}
